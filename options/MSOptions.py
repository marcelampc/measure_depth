import argparse
import os

# Measure and Save options

class MSOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        # self.initialized = 

    def initialize(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--data_root', help='root')
        self.parser.add_argument('--path_gt', help='path to ground truth \
                            files')
        self.parser.add_argument('--path_pred', help='path to label files')
        self.parser.add_argument('--results_path', default='./results')
        self.parser.add_argument('--epoch', default='latest')
        self.parser.add_argument('--up', default=0.8, help='up threshold to accuracy')
        self.parser.add_argument('--down', default=0.05, help='up threshold to accuracy')
        self.parser.add_argument('--name', help='if using data_root: name of the experiment, else: root name of the saved images')
        self.parser.add_argument('--plot', action='store_true')
        self.parser.add_argument('--phase', default='test')
        self.parser.add_argument('--bad_minmax', action='store_true', help='use bad min and max to calculate error')
        self.parser.add_argument('--original_resolution', action='store_true', help='adp')
        self.parser.add_argument('--resample_technique', default='bicubic', help='Options to resample: nearest, bilinear, cubic, bicubic (default)')
        # from gray to cmap
        self.parser.add_argument('--filename')
        self.parser.add_argument('--cmap', default='jet')
        self.parser.add_argument('--interpolation', default='spline16')
        self.parser.add_argument('--print_options', action='store_true')
        # opt = self.parser.parse_args()
        # print(opt)


    def parse(self):
        self.initialize()
        self.opt = self.parser.parse_args()

        if self.opt.print_options:
            # print information in a nice way:
            args = dict((arg, getattr(self.opt, arg)) for arg in dir(self.opt) if not arg.startswith('_'))
            print('---Options---')
            for k, v in sorted(args.items()):
                print('{}: {}'.format(k, v))

        return self.opt
