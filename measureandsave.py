# Measure error, get images from folder, transform to color and save in chosen folder

# common opt file
# get options
from options.MSOptions import MSOptions
from modules.errormeasurement import ErrorMeasure
from modules.graytocmap import GrayCMap
opt = MSOptions().parse()

from ipdb import set_trace as st

# Measure error
errorob = ErrorMeasure(opt)
errorob.initialize()
errorob.calculate_all()
# errorob.calculate_per_iter()
# ranking_dict = errorob.get_ranking_list()

# gets names to color and save
# ranked_names_list = [name for name, value in ranking_dict]

# gcm_obj = GrayCMap(opt)
# gcm_obj.initialize()
# gcm_obj.save_from_list(ranked_names_list)


# save a vector with the name of the best to the worst measurements
# which to pic? Precision?

# first: save a vector with the best and worst
